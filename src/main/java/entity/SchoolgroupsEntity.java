package entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "schoolgroups", schema = "schoolmain")
public class SchoolgroupsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "peoplesamount", nullable = true)
    private Integer peoplesamount;
    @Column(name = "headman_id", nullable = true)
    private Integer headmanId;
    @Column(name = "groupname", nullable = true, length = 5)
    private String groupname;
    @ManyToMany(cascade = {CascadeType.ALL, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "schoolgroups_teachers",
                joinColumns = @JoinColumn(name = "id_schoolgroups"),
                inverseJoinColumns = @JoinColumn(name = "id_teachers")
    )
    List<TeachersEntity> teachers;
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SchoolgroupsEntity that = (SchoolgroupsEntity) o;
        return Objects.equals(id, that.id);
    }
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}