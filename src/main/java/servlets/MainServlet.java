package servlets;

import dao.SchoolgroupsDAO;
import dao.TeachersDAO;
import entity.SchoolgroupsEntity;
import entity.TeachersEntity;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainServlet extends HttpServlet {
    private final TeachersDAO teachersDAO = new TeachersDAO();
    private final SchoolgroupsDAO schoolgroupsDAO = new SchoolgroupsDAO();
    static Logger logger;
    private static final String EXCEPTIONMSG = "ExceptionMain: ";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
            req.setCharacterEncoding("UTF-8");
            String groupNumber = req.getParameter("groupNumber");
            String groupName = req.getParameter("groupNames");
            String group = groupNumber + groupName;
            Integer peoplesAmount = Integer.valueOf(req.getParameter("peoplesAmount"));
            String[] teacherID = req.getParameterValues("teachers");
            SchoolgroupsEntity schoolgroups = new SchoolgroupsEntity(null, peoplesAmount, null, group, null);
            List<TeachersEntity> teachersList = new ArrayList<>();
            for (String id: teacherID) {
                teachersList.add(teachersDAO.getById(Integer.parseInt(id)));
            }
            schoolgroups.setTeachers(teachersList);
            schoolgroupsDAO.save(schoolgroups);
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/"));
        } catch (Exception e){
            logger.log(Level.INFO,EXCEPTIONMSG, e);
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
            req.setAttribute("schoolgroups", schoolgroupsDAO.getAll());
            req.setAttribute("teachers", teachersDAO.getAll());
            getServletContext().getRequestDispatcher("/index.jsp").forward(req, resp);
        } catch (Exception e) {
            logger.log(Level.INFO,EXCEPTIONMSG, e);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int deleteGroupById;
            deleteGroupById = Integer.parseInt(req.getParameter("id"));
            schoolgroupsDAO.remove(schoolgroupsDAO.getById(deleteGroupById));
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/"));
        } catch (Exception e) {
            logger.log(Level.INFO,EXCEPTIONMSG, e);
        }
    }
}
