package servlets;

import dao.SchoolgroupsDAO;
import dao.TeachersDAO;
import entity.SchoolgroupsEntity;
import entity.TeachersEntity;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EditServlet extends HttpServlet {
    private final TeachersDAO teachersDAO = new TeachersDAO();
    private final SchoolgroupsDAO schoolgroupsDAO = new SchoolgroupsDAO();
    static Logger logger;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String idSchoolGroup = req.getParameter("id");
            String groupNumber = req.getParameter("groupNumber");
            String groupName = req.getParameter("groupNames");
            String group = groupNumber + groupName;
            Integer peoplesAmount = Integer.valueOf(req.getParameter("peoplesAmount"));
            String[] teacherID = req.getParameterValues("teachers");
            SchoolgroupsEntity schoolgroups = new SchoolgroupsEntity(Integer.parseInt(idSchoolGroup), peoplesAmount, null, group, null);
            List<TeachersEntity> teachersList = new ArrayList<>();
            for (String id: teacherID) {
                teachersList.add(teachersDAO.getById(Integer.parseInt(id)));
            }
            schoolgroups.setTeachers(teachersList);
            int version= Integer.parseInt(idSchoolGroup);
            schoolgroups.setId(version);
            schoolgroupsDAO.save(schoolgroups);
            resp.sendRedirect("/");
        } catch (Exception e){
            logger.log(Level.INFO,"Exception: ", e);
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String id = req.getParameter("id");
            req.setAttribute("schoolgroups", schoolgroupsDAO.getById(Integer.parseInt(id)));
            req.setAttribute("teachers", teachersDAO.getAll());
            getServletContext().getRequestDispatcher("/EditPage.jsp").forward(req, resp);
        }catch (Exception e){
            logger.log(Level.INFO,"Exception: ", e);
        }

    }
}

