package servlets;

import dao.SchoolgroupsDAO;
import dao.TeachersDAO;
import entity.SchoolgroupsEntity;
import entity.TeachersEntity;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class FilterServlet extends HttpServlet {
    private final TeachersDAO teachersDAO = new TeachersDAO();
    private final SchoolgroupsDAO schoolgroupsDAO = new SchoolgroupsDAO();
    private static List<SchoolgroupsEntity> schoolgroupsList;
    static Logger logger;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String teacherid = req.getParameter("teacherFio");
            schoolgroupsList = schoolgroupsDAO.getAll();
            TeachersEntity teacher = teachersDAO.getById(Integer.parseInt(teacherid));
            schoolgroupsList.removeIf(schoolgroupsEntity -> !schoolgroupsEntity.getTeachers().contains(teacher));
            doGet(req, resp);
        } catch (Exception e) {
            logger.log(Level.INFO,"Exception: ", e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            req.setAttribute("schoolgroups", schoolgroupsList);
            req.setAttribute("teachers", teachersDAO.getAll());
            getServletContext().getRequestDispatcher("/index.jsp").forward(req, resp);
        } catch (Exception e) {
            logger.log(Level.INFO,"Exception: ", e);
        }
    }
}
