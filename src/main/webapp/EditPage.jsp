<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Task_4</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="container">
    <form method="post" action="" id="editForm">
        <input type="hidden" name="id" value="${schoolgroups.id}" />
        <select name="groupNumber" id="groupNumberId" >
            <option value="1" <c:if test="${fn:contains(schoolgroups.groupname, '1')}"> selected </c:if>>1</option>
            <option value="2" <c:if test="${fn:contains(schoolgroups.groupname, '2')}"> selected </c:if>>2</option>
            <option value="3" <c:if test="${fn:contains(schoolgroups.groupname, '3')}"> selected </c:if>>3</option>
            <option value="4" <c:if test="${fn:contains(schoolgroups.groupname, '4')}"> selected </c:if>>4</option>
            <option value="5" <c:if test="${fn:contains(schoolgroups.groupname, '5')}"> selected </c:if>>5</option>
            <option value="6" <c:if test="${fn:contains(schoolgroups.groupname, '6')}"> selected </c:if>>6</option>
            <option value="7" <c:if test="${fn:contains(schoolgroups.groupname, '7')}"> selected </c:if>>7</option>
            <option value="8" <c:if test="${fn:contains(schoolgroups.groupname, '8')}"> selected </c:if>>8</option>
            <option value="9" <c:if test="${fn:contains(schoolgroups.groupname, '9')}"> selected </c:if>>9</option>
            <option value="10" <c:if test="${fn:contains(schoolgroups.groupname, '10')}"> selected </c:if>>10</option>
            <option value="11" <c:if test="${fn:contains(schoolgroups.groupname, '11')}"> selected </c:if>>11</option>
        </select>
        <select name="groupNames" id="groupNamesId" >
            <option value="A" <c:if test="${fn:contains(schoolgroups.groupname, 'A')}"> selected </c:if>>A</option>
            <option value="B" <c:if test="${fn:contains(schoolgroups.groupname, 'B')}"> selected </c:if>>B</option>
            <option value="C" <c:if test="${fn:contains(schoolgroups.groupname, 'C')}"> selected </c:if>>C</option>
            <option value="D" <c:if test="${fn:contains(schoolgroups.groupname, 'D')}"> selected </c:if>>D</option>
        </select>
        <input type="text" class="peoplesAmount" name="peoplesAmount" placeholder="Enter number of peoples" value="${schoolgroups.peoplesamount}">
        <select name="teachers" multiple>
            <c:forEach var="teacher" items="${teachers}">
                <option value="${teacher.id}">${teacher.teacherfio}</option>
            </c:forEach>
        </select><br>
        <input type="submit" value="Add" class="btn addBtn"><br>
        <a href="/">Back to main page</a>
    </form>
</div>


</body>
</html>
