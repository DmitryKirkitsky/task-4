<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="style.css">
    <title>Task_4</title>
</head>
<body>
<div class="container">
    <header>
        <h1>Web Application on JSP</h1>
    </header>
    <main>
        <button class="btn createBtn" onclick="">Create</button>
        <div class="row">
            <div class="col-6">
                <table border="1" class="table">
                    <caption>School Groups</caption>
                    <tr>
                        <th scope="col">Group Name</th>
                        <th scope="col">Peoples Amount</th>
                        <th scope="col">Teacher FIO</th>
                        <th scope="col">Action</th>
                    </tr>
                    <c:forEach var="schoolgroup" items="${schoolgroups}">
                        <tr>
                            <td>${schoolgroup.groupname}</td>
                            <td>${schoolgroup.peoplesamount}</td>
                            <td><c:forEach var="teacher" items="${schoolgroup.teachers}">${teacher.teacherfio} </c:forEach></td>
                            <td><button class="tableBtn" onclick="location.href='http://localhost:8080/EditPage?id=${schoolgroup.id}'">Edit</button> / <button class="tableBtn" onclick="send(${schoolgroup.id})">Delete</button></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>

            <div class="col-6">
                <h2>Filters</h2>
                <form method="post" action="filter">
                    <select name="teacherFio">
                        <option value=""></option>
                        <c:forEach var="teacher" items="${teachers}">
                            <option value="${teacher.id}">${teacher.teacherfio}</option>
                        </c:forEach>
                    </select>
                    <input type="submit" value="Filter" class="btn filterBtn"/>
                </form>
                <button onclick="location.href='/'" class="btn filterBtn">Drop filter</button>
            </div>
        </div>
            <form method="post" id="addForm" action="">
                <select name="groupNumber" id="groupNumberId">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                </select>
                <select name="groupNames" id="groupNamesId" >
                    <option value="A">A</option>
                    <option value="B">B</option>
                    <option value="C">C</option>
                    <option value="D">D</option>
                </select>
                <input type="text" class="peoplesAmount" name="peoplesAmount" placeholder="Enter number of peoples">
                <select name="teachers" multiple>
                    <c:forEach var="teacher" items="${teachers}">
                        <option value="${teacher.id}">${teacher.teacherfio}</option>
                    </c:forEach>
                </select><br>
                <input type="submit" value="Add" class="btn addBtn">
            </form>
</main>
</div>
</body>
</html>

<script>
    function send(id) {
        fetch("http://localhost:8080/?id=" + id, {method: 'DELETE'});
        window.location.reload();
    }
    let createBtn = document.getElementsByClassName("createBtn")[0];
    let form = document.getElementById("addForm");
    let addBtn = document.getElementById("addBtn");

    createBtn.onclick = function() {
        form.style.display = 'block';
    }

</script>